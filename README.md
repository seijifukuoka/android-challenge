
# Skeelo Recrutamento

---
![JPG](https://app.skeelo.com/conheca-o-skeelo/img/ebook-illustration-1.png)


## Bem vindo

Obrigado por participar do desafio do Skeelo! Estamos muito contentes pelo seu primeiro passo para fazer parte de um time excepcional. 

Fique a vontade para utilizar qualquer API, vamos deixar algumas sugestões abaixo:

- [TheMovieDB](https://developers.themoviedb.org/3/getting-started/introduction)
- [Marvel](https://developer.marvel.com/)
- [Pokemon](https://pokeapi.co/docs/v2)

Você deverá criar um aplicativo, usando alguma API citada acima ou qualquer outra de sua preferência, atendendo as funcionalidades listadas abaixo.
Para termos um layout minimamente definido, anexamos alguns assets que vão te ajudar a desenvolver esse app:

- [ScreenShots](assets/screenshots)
- [Ícones](assets/icons)

Pense no desafio como uma oportunidade de mostrar todo o seu conhecimento. E faça com calma, você tem uma semana para entregar! Sua avaliação será baseada nos seguintes tópicos:

- Arquitetura
- Consumo de APIs
- Layout's
- Fluxo de navegação
- Qualidade do código desenvolvido

### Linguagem

- Kotlin

### Bibliotecas

- Sinta-se a vontade em usar ou não usar bibliotecas


# Funcionalidades

---

### Essencial:

- Listagem dos items
- Detalhes do item clicado a partir da lista
- Tratamento de erro

### Ganha mais pontos se tiver:

- Funcionamento offline
- Paginação
- Testes unitários

### Iremos ficar encantados:
- Algo inesperado e surpreendente

# Exemplos e sugestões

---

Nossos designers elaboraram algumas sugestões de telas e fluxos para guiar você durante o desenvolvimento, portanto fique à vontade para modificar como você quiser.
Para facilitar o processo, existem assets, app icons, ícones e paleta de cores no repositório. Mas se o seu lado designer falar mais alto, pode nos surpreender!

- [Splash](assets/screenshots/splash.png)
- [Grid de filmes](assets/screenshots/home.jpg)
- [Detalhes](assets/screenshots/highlights-details.jpg)
- [Assista Também](assets/screenshots/highlights.jpg)

-> Outras inspirações

- [Detalhes](assets/screenshots/detail.png)
- [Em_breve](assets/screenshots/upcoming.png)
- [Populares](assets/screenshots/popular.png)

## **Processo de entrega**

---

Após finalizar a implementação do desafio, abra um pull request para este repositório seguindo os passos abaixo:

1. Faça um fork deste repositório, não clone este repositório diretamente
2. Os commit's e as alterações devem ser feitas no **SEU** fork
3. Envie um Pull Request
4. Deixe o fork público para facilitar a inspeção do código

### **ATENÇÃO**

Não faça push diretamente para este repositório!